package net.javaguides.springboot.workqueues.consumer;

import net.javaguides.springboot.workqueues.dto.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQTopic2Consumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQTopic2Consumer.class);

    @RabbitListener(queues = {"${rabbitmq.queue.topic.name2}"})
    public void consumeJsonMessage(Employee employee) {
        LOGGER.info(String.format("Received Topic message -> %s", employee));
    }
}
