package net.javaguides.springboot.workqueues.dto;

import lombok.Data;

@Data
public class Employee {
    private int id;
    private String firstName;
    private String lastName;
}
