package net.javaguides.springboot.workqueues.controller;

import net.javaguides.springboot.workqueues.dto.Employee;
import net.javaguides.springboot.workqueues.publisher.RabbitMQTopicProducer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class MessageTopicController {

    private RabbitMQTopicProducer topicProducer;

    public MessageTopicController(RabbitMQTopicProducer topicProducer) {
        this.topicProducer = topicProducer;
    }

    @PostMapping("topic/publish")
    public ResponseEntity<String> sendJsonMessage(@RequestBody Employee employee) {
        topicProducer.sendMessageToTopic(employee);
        return ResponseEntity.ok("Topic message sent to RabbitMQ....");
    }

}
