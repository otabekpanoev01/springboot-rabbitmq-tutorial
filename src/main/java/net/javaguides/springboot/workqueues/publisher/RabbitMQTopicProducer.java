package net.javaguides.springboot.workqueues.publisher;

import net.javaguides.springboot.workqueues.dto.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQTopicProducer {

    @Value("${rabbitmq.topic.exchange.name}")
    private String exchange;

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQTopicProducer.class);

    private RabbitTemplate rabbitTemplate;

    public RabbitMQTopicProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessageToTopic(Employee employee) {
        LOGGER.info(String.format("Topic message sett -> %s", employee.toString()));
        rabbitTemplate.convertAndSend(exchange, "", employee);
    }
}
