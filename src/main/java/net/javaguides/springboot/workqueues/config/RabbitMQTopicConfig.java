package net.javaguides.springboot.workqueues.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQTopicConfig {
    @Value("${rabbitmq.queue.topic.name1}")
    private String topicQueue1;

    @Value("${rabbitmq.queue.topic.name2}")
    private String topicQueue2;

    @Value("${rabbitmq.topic.exchange.name}")
    private String exchange;

//    @Value("${rabbitmq.routing.topic.key}")
//    private String routingTopicKey;

    @Bean
    public Queue queue1() {
        return new Queue(topicQueue1);
    }

    @Bean
    public Queue queue2() {
        return new Queue(topicQueue2);
    }

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange(exchange);
    }

    @Bean
    public Binding bindingTopic1() {
        return BindingBuilder
                .bind(queue1())
                .to(fanoutExchange());
//                .with(routingTopicKey);
    }

    @Bean
    public Binding bindingTopic2() {
        return BindingBuilder
                .bind(queue2())
                .to(fanoutExchange());
//                .with(routingTopicKey);
    }

}
