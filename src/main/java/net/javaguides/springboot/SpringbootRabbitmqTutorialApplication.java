package net.javaguides.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;

@SpringBootApplication
public class SpringbootRabbitmqTutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootRabbitmqTutorialApplication.class, args);
	}

}
